# OpenMP

C'est une bibliothèque disponible en C/C++ (Fortran également) afin de permettre la parallélisation par processus légers (threads). La version actuelle **5** est disponnible directement dans les compilateurs gcc et g++.


Quelques liens de documentation utile:
- https://www.openmp.org/spec-html/5.0/openmp.html
- https://docs.microsoft.com/fr-fr/cpp/parallel/openmp/openmp-in-visual-cpp?view=msvc-160

## Parallélisation simple

On va dans un premier temps paralléliser un Hello World pour qu'il se fasse sur chaque thread. OpenMP est simple d'utilisation, le header `omp.h` contient tout le nécessaire. Il ne reste qu'à indiquer à OpenMP qu'il faut paralléliser une instruction : `pragma omp parallel`.
```cpp
#include <cstdio>
#include <omp.h>

int main()
{
    #pragma omp parallel
    printf("Hello World !\n");
}
```

En compilant et en exécutant ce code vous allez remarquer que le `Hello World !` n'est affiché qu'une fois. C'est parce qu'on n'a pas demandé au compilateur d'utiliser OpenMP.
```bash
g++ main.cpp -fopenmp
```

En exécutant une nouvelle fois, on va remarquer que le code est exécuté sur chaque thread : il y a une ligne par thread disponible sur la machine.

{% hint style="info" %}
La clause de parallélisation est effective sur le bloc d'instructions qui suit. Pour inclure plusieurs instructions dans le même bloc parallèle, il faut les entourer d'accolades.
```cpp
#pragma omp parallel
{
    printf("Hello ");
    printf("World !\n");
}
```
Ce fonctionnement est valable sur l'ensemble des directives d'OpenMP.
{% endhint %}

## Afficher la répartition des threads

C'est bien gentil de faire du travail parallèle, mais c'est pas facile de s'y retrouver. Affichons le nombre de threads utilisés ainsi que l'identifiant de chaque thread.

```cpp
int main()
{
    int thread_id{-1};
    int threads_count{-1};
    #pragma omp parallel
    {
        thread_id = omp_get_thread_num();       // Identifiant du thread
        threads_count = omp_get_num_threads();  // Nombre de threads utilisés
        printf("Hello World ! (thread %d/%d)\n", thread_id, threads_count);
    }
}
```
Les identifiants de threads peuvent être affichés de manière erronée, il faut spécifier la manière dont les threads partagent les variables (to be continued...).

## Configuration de l'environnement parallèle

### Partage de la mémoire

Le plus difficile lorsque l'on fait de la programmation parallèle est d'établir les règles de partage de la mémoire. OpenMP fournit une approche simple grâce à quelques mots clés extrait de la [documentation de Microsoft](https://docs.microsoft.com/fr-fr/cpp/parallel/openmp/reference/openmp-clauses?view=msvc-160). Il existe d'autres possibilités plus complexes.

| Clause | Signification |
| --- | --- |
| `private` | Chaque thread possède sa propre instance de la variable, non initialisée. |
| `firstprivate` | Chaque thread possède sa propre instance de la variable, initialisée à la valeur avant parallélisation. |
| `shared` | Chaque thread partage la même zone mémoire. |

On indique le comportement de chaque variable dans la directive parallel.

```cpp
#pragma omp parallel firstprivate(a) private(b, c) shared(d)
```

On peut donc corriger l'affichage des identifiants.
```cpp
int main()
{
    int thread_id{-1};
    int threads_count{-1};
    #pragma omp parallel private(thread_id)
    {
        thread_id = omp_get_thread_num();       // Identifiant du thread
        threads_count = omp_get_num_threads();  // Nombre de threads utilisés
        printf("Hello World ! (thread %d/%d)\n", thread_id, threads_count);
    }
}
```

{% hint style="info" %}
Lorsque rien n'est spécifié les index d'itération des boucles ainsi que les variables utilisée depuis la partie parallèle sont considérés `private`. Toutes les autres variables sont considérées comme `shared` à savoir les variables globales, statiques ou les variables utilisée avant la partie parallèle.
{% endhint %}

Il est possible de forcer cette spécification de comportement afin d'éviter les erreurs.
```cpp
#pragma omp default(none)
```
On revient au comportement précédent en indiquant `shared`.
```cpp
#pragma omp default(shared)
```

{% hint style="danger" %}
Il n'est pas possible d'indiquer un autre mode par défaut.
{% endhint %}

Si le partage de la mémoire de cette manière ne convient pas au besoin du programme. Il reste toujours la possibilité d'utiliser des mutex.
```cpp
omp_lock_t mutex;       // Création du mutex
omp_init_lock(&mutex);  // Initialisation du mutex (C'est du C donc pas de constructeur)

omp_set_lock(&mutex);   // Prise du mutex
/*
 Code non parallèle
*/
omp_unset_lock(&mutex); // Libération du mutex

omp_destroy_lock(&mutex); // Destruction du mutex (C'est du C donc pas de destructeur)
```

#### Synchronisation de la mémoire

Il est possible que les threads contiennent des valeurs différentes des variables shared. Pour s'assurer qu'ils contiennent tous la même chose on utilise les `flush`. L'exemple provient de la [documentation de microsoft](https://docs.microsoft.com/fr-fr/cpp/parallel/openmp/reference/openmp-directives?view=msvc-160#flush-openmp)

```cpp

void read(int *data) {
   printf_s("read data\n");
   *data = 1;
}

void process(int *data) {
   printf_s("process data\n");
   (*data)++;
}

int main() {
   int data;
   int flag;

   flag = 0;

   #pragma omp parallel sections num_threads(2)
   {
      #pragma omp section
      {
         printf_s("Thread %d: ", omp_get_thread_num());
         read(&data);
         #pragma omp flush(data)
         flag = 1;
         #pragma omp flush(flag)
         // Do more work.
      }

      #pragma omp section
      {
         while (!flag) {
            #pragma omp flush(flag)
         }
         #pragma omp flush(data)

         printf_s("Thread %d: ", omp_get_thread_num());
         process(&data);
         printf_s("data = %d\n", data);
      }
   }
}
```

### Configurer le nombre de threads

Il est possible de spécifier combien de threads doivent être utilisés pour exécuter une section parallèle. Cette précision est possible à plusieurs échelles.

1. Tout d'abord lors de l'exécution on peut donner le nombre maximum de threads alloués au programe. Pour cela on utilise la variable d'environnement `OMP_NUM_THREADS`.
2. On peut spécifier le nombre de threads maximum lors du runtime avec la fonction `omp_set_num_threads(int nbThreads)`.
3. Enfin il est possible d'indiquer directement dans la directive le nombre de threads à utiliser `#pragma omp parallel num_threads(nbThreads)`.

{% hint style="info" %}
Les manières de configurer le nombre de threads ont été donné dans l'ordre de la moins à la plus prioritaire.
{% endhint %}


## Parallélisation de plusieurs blocs
Il est possible de créer des blocs de codes devant être exécuté en parallèle. Pour cela, on utilise des `sections`. On indique tout d'abord que l'on est dans un environnement de sections, puis on décrit chaque section.
```cpp
int main()
{
    int thread_id{-1};
    int threads_count{-1};
    #pragma omp parallel private(thread_id, threads_count)
    {
        #pragma omp sections
        {
            #pragma omp section
            {
                thread_id = omp_get_thread_num();
                threads_count = omp_get_num_threads();
                printf("[S1] Hello World ! (thread %d/%d)\n", thread_id, threads_count);   
            }
            
            #pragma omp section
            {
                thread_id = omp_get_thread_num();
                threads_count = omp_get_num_threads();
                printf("[S2] Hello World ! (thread %d/%d)\n", thread_id, threads_count);   
            }
            
            #pragma omp section
            {
                thread_id = omp_get_thread_num();
                threads_count = omp_get_num_threads();
                printf("[S3] Hello World ! (thread %d/%d)\n", thread_id, threads_count);   
            }
        }
    }
}
```

On obtient dans l'exemple précédent que seules 3 lignes sont affichées comme seulement 3 sections sont présentes.

## Parallélisation de boucles
Créer des sections peut être une tâche répétitive (et non scalable). OpenMP fournit de quoi paralléliser des boucles `for`.
L'exemple précédent est donc beaucoup plus simple à écrire.

```cpp
int main()
{
    int thread_id{-1};
    int threads_count{-1};
    #pragma omp parallel private(thread_id, threads_count)
    {
        #pragma omp for
        for (unsigned i = 1; i < 4; ++i)
        {
            thread_id = omp_get_thread_num();
            threads_count = omp_get_num_threads();
            printf("[S%d] Hello World ! (thread %d/%d)\n", i, thread_id, threads_count);   
        }
    }
}
```

### Sheduler
 Il est possible d'indiquer à OpenMP la manière dont il doit attribuer des threads des boucles parallèles. Voila un blog avec des schémas clairs sur le fonctionnement de chacun : [jakascorner.com](http://jakascorner.com/blog/2016/06/omp-for-scheduling.html).

Pour certains, on peut préciser combien d'itérations chaque thread doit faire. Lorsque le thread a fini ses itérations, il peut être réutilisé pour faire d'autres itérations.

| Scheduler | Description | Sans préciser le nombre d'itération | Conditions de sélection 
| --- | --- | --- | --- |
| static | Le premier thread prend les premières itérations, le second enchaine etc... | La boucle est découpée en *nbthreads* sous parties. | Les itérations sont toutes équivalentes en coût. |
| dynamic | Les groupes d'itérations sont réparties au runtime sur les threads disponibles. | Les itérations sont distribuées une à une. | Les itérations ne sont pas du tout équivalentes en coût. 
| guided | Comme le dynamic mais la taille des groupes dépend du nombre restant d'itérations (proportionnel), le paramêtre correspond à la taille minimale qu'ils peuvent avoir. | A la fin les itérations sont distribuées une à une. | Même objectif que dynamic, mais réduit les coups de changement de contexte.
| auto | Le compilateur ou le sytème d'exploitation décide de l'approche à utiliser. | Le paramètre est tout le temps ignoré. | Quand on ne peut pas faire de mesure sur quel scheduler est le plus efficace. |
| runtime | La décision est prise au runtime avec l'utilisation de la variable d'environnement : `OMP_SCHEDULE` ou de la fonction `omp_set_schedule()`.

## Utilisation d'un seul thread

La création et la fin d'un bloc parallèle coûtent cher en performance, il faut donc l'éviter au maximum. Lorsqu'une tâche non parallélisable doit être faite par un seul thread on peut utiliser une directive d'OpenMP.

```cpp
int main()
{
    int thread_id{-1};
    int threads_count{-1};
    #pragma omp parallel private(thread_id, threads_count) num_threads(4)
    {
        thread_id = omp_get_thread_num();
        threads_count = omp_get_num_threads();
        printf("Hello World ! (thread %d/%d)\n", thread_id, threads_count);
        
        #pragma omp single private(thread_id, threads_count)
        {
            thread_id = omp_get_thread_num();
            threads_count = omp_get_num_threads();
            printf("Bye bye ! (thread %d/%d)\n", thread_id, threads_count);
        } 
        
        thread_id = omp_get_thread_num();
        threads_count = omp_get_num_threads();
        printf("Hello World ! (thread %d/%d)\n", thread_id, threads_count);
    }
}
```

Le thread utilisé pour le bloc `single` n'est jamais certain. Il peut changer d'une exécution à l'autre. OpenMP fournit le `pragma omp master` qui fonctionne de la même manière mais seul le thread principal du programme pourra exécuter le code.

## Synchronisation temporelle

Il est possible de forcer les threads à attendre qu'ils aient tous atteint un point du programme. Il faut pour celà utiliser une `barrier`. Dans l'exemple suivant, la barrière permet de garantir que les 4 threads auront fait le premier affichage, avant qu'aucun ne fasse le second.
```cpp
int main()
{
    int thread_id{-1};
    int threads_count{-1};
    #pragma omp parallel private(thread_id, threads_count) num_threads(4)
    {
        thread_id = omp_get_thread_num();
        threads_count = omp_get_num_threads();
        printf("[1] Hello World ! (thread %d/%d)\n", thread_id, threads_count);
        
        #pragma omp barrier
        
        printf("[2] Hello World ! (thread %d/%d)\n", thread_id, threads_count);
    }
}
``` 

Lorsqu'une portion de code peut poser des problèmes de partage et/ou de synchronisation de la mémoire, on peut forcer les threads à passer les uns après les autres. C'est évidemment à éviter le plus possible parce que cela crée des goulots d'étranglement et donc des réductions de performances.
```cpp
#pragma omp parallele
{
    // Pas de limitations

    #pragma omp critical
    {
        // Exécution des threads un par un
    }

    // Pas de limitations
}
```

Enfin on peut demander qu'une portion de code n'attende pas que la précédente soit finie pour commencer à s'exécuter : `nowait`. Exemple provenant de la [documentation de microsoft](https://docs.microsoft.com/fr-fr/cpp/parallel/openmp/reference/openmp-clauses?view=msvc-160#default-openmp)
```cpp
#define SIZE 5

void test(int *a, int *b, int *c, int size)
{
    int i;
    #pragma omp parallel
    {
        #pragma omp for nowait
        for (i = 0; i < size; i++)
            b[i] = a[i] * a[i];

        #pragma omp for nowait
        for (i = 0; i < size; i++)
            c[i] = a[i]/2;
    }
}

int main()
{
    int a[SIZE], b[SIZE], c[SIZE];
    int i;

    for (i=0; i<SIZE; i++)
        a[i] = i;

    test(a,b,c, SIZE);

    for (i=0; i<SIZE; i++)
        printf("%d, %d, %d\n", a[i], b[i], c[i]);
}
```

## Mesures

Il est possible de facilement mesurer le temps entre deux instants dans les threads. Ce temps correspond au temps que l'utilisateur a passé à attendre le résultat.
```cpp
int main()
{
    #pragma omp parallel num_threads(4)
    {
        double start; 
        double end; 
        double resultat = 1;
        start = omp_get_wtime();
        for (unsigned i = 0; i < 100'000'000; ++i)
            resultat = 3 * (resultat + 5);
        end = omp_get_wtime(); 
        printf("Work took %f seconds\n", end - start);
    }
}
```

On peut utiliser la fonction `omp_get_wtick()` pour obtenir le temps réel passé par le thread sur le calcul. On ignore le temps pendant lequel le processeur à mis en pause notre programme.

## Reduction

Les réductions permettent à OpenMP d'accélérer le traitement de variables partagées en indiquand un traitement final (fin de parallélisation). C'est une manière particulière de partager les variables entre les threads.

```cpp
int main()
{
    int sum = 0;
    #pragma omp parallel for reduction(+ : sum)
    for (int i = 0; i < 30; ++i) // Somme des nombres de fibo jusqu'au 30eme.
        sum += fibonacci(i);   
}
```

Les opérations binaires commutatives sont possibles pour les réductions : 
- mathématiques : `+`, `*`, `-`, `max`, `min`.
- logiques : `&&`, `||`.
- binaires : `&`, `|`, `^`.
