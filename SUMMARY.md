# Summary

* [Introduction](README.md)
# CI/CD
* [Docker](ci_cd/docker.md)
* [Dockerfile](ci_cd/dockerfile.md)
* [Docker-Compose](ci_cd/docker_compose.md)
# Outils
* [Git](tools/git.md)
* [Android Command Line](tools/android_cmd_line.md)
# Programmation
## C++
### Bibliothèques
* [OpenMP](cpp/lib/openmp.md)
### Outils
* [Conan](cpp/tools/conan.md)

