# Dockerfile

Le dockerfile est un fichier permettant de configurer sa propre image docker. On va parcourir ici les étapes à suivre pour créer s'occupant de la compilation de programme en C++ utilisant CMake.

Le fichier à créer s'appelle tout simplement "Dockerfile".
{% hint style="info" %}
Le format permet d'utiliser des commentaires.
```dockerfile
# P'tit commentaire qui fait zizir.
```
{% endhint %}

## Base

Lorsqu'on crée une image, on commence par chercher une image qui contient les éléments que l'on souhaite : quelqu'un a potentiellement déjà une image proche de notre besoin. Pour le cas présent on reste sur une image Debian simple pour que l'on ai quelques préparations à faire.

```dockerfile
FROM debian:10
```

Cette image source est-elle même basée sur Debian 10, ici c'est simple de deviner mais parfois c'est pratique de connaître cette astuce : `cat /etc/*-release`.

## Exécution de commandes

Comme l'image est basée sur Debian on sait que `apt` est disponnible.
On peut supposer que apt est une source sûre pour la provenance de binaire dans le sens où les dépôts ne seront pas arrêter de si tôt. On va donc l'utiliser pour télécharger ce dont on peut avoir besoin. 

{% hint style="danger" %}
La version du programme installée peut évoluer si on a besoin de refaire l'image un jour, spécifier la version de chaque programme peut-être intéressant.
{% endhint %}

On installe tout d'abord le nécessaire pour la compilation.
```dockerfile
RUN apt update && apt install -y g++ cmake
```

Chaque commande `RUN` est exécutée dans un processus séparé, il ne fonctionnera donc pas de faire des changements d'état comme des `cd` d'une ligne à l'autre. C'est pour cette raison que l'on utilise le `&&` pour exécuter plusieurs commandes fonctionnant ensembles.

{% hint style="info" %}
La commande `cd` en particulier, n'est pas un problème. Une directive existe pour que toute les commandes suivantes soient exécutées à partir d'un dossier.
```dockerfile
WORKDIR /home/
```
{% endhint %}


Pour éviter de faire la compilation en tant que root, on ajoute un utilisateur `compiler`.
```dockerfile
RUN useradd -ms /bin/bash compiler
```
## Connexion par défaut
Pour éviter que par défaut la connexion se fasse sur l'utilisateur `root` on peut spécifer l'utilisateur par défaut comme étant `compiler`.
```dockerfile
USER compiler
```

## Utilisation de variables d'environnement

On peut également ajouter ou modifier les variables d'environnement du conteneur. On va en créer une pour indiquer le dossier du conteneur qui sera la racine du projet à compiler.
```dockerfile
ENV PROJECT_ROOT=/home/compiler/project_root
```
On peut également utiliser cette variable dans la suite du fichier pour faire d'autres configurations.

## Compilation automatique
L'objectif du conteneur étant de compiler automatiquement le projet CMake, on va changer le comportement par défaut de notre image. Actuellement c'est le programme `/bin/bash` qui est lancé. Remplaçons ça par `cmake` et `make`.

On peut pour celà utiliser `CMD` ou `ENTRYPOINT` plus de détail [ici](https://aws.amazon.com/fr/blogs/france/demystifier-entrypoint-et-cmd-dans-docker/).
```dockerfile
CMD cd $PROJECT_ROOT/build && cmake .. && make -j
```

## Contexte de création
Lors de la création d'une image, docker ajoute dans le contexte tous les fichiers et dossiers présents au même endroit que le dockerfile. Ce contexte peut rapidement devenir trop gros, on peut donc créer un fichier `.dockerignore` qui fonctionne comme un `.gitignore`.

L'intérêt de ce contexte est de pouvoir ajouter des fichiers dans l'image.
Pour celà on peut utiliser `COPY` ou `ADD`. Le premier se contente de copier un fichier ou un dossier de la machine locale. Le second est plus puissant car il permet également d'obtenir des fichiers distants via une URL ou de décrompresser des archives tar directement dans l'image.

## Ports de connexion
Il est possible de spécifier directement dans le Dockerfile quels ports sont censés être ouverts lors de la création d'un conteneur. Ici on indique que le port 22 devrait être ouvert.
```dockerfile
EXPOSE 22
```
